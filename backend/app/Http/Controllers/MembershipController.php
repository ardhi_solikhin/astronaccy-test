<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

use App\Membership;
use App\Http\Resources\Membership as MembershipResource;
use App\Http\Resources\MembershipCollection;

use App\MemberType;

class MembershipController extends Controller
{
    public function index()
    {
        return new MembershipCollection(Membership::all());
    }

    public function show($id)
    {
        return new MembershipResource(Membership::findOrFail($id));
    }

    public function store(Request $request)
    {
        $file = $request->file('attachment');
        $name = time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('attachment', $name);

        $membership = Membership::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'attachment' => $path,
            'type' => $request->type,
            'api_token' => Str::random(60)
        ]);

        return response()
            ->json(['message' => 'success'])
            ->setStatusCode(200);
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $data = Membership::where('email', $email)->first();
        if($data) {
            if(Hash::check($password, $data->password)){
                Session::put('name',$data->name);
                Session::put('email',$data->email);
                Session::put('login',TRUE);
                
                $member_type = MemberType::where('type', $data->type)->get();
                $set_data = array(
                    'user' => $data,
                    'member' => $member_type
                );

                return (new MembershipResource($set_data))
                    ->response()
                    ->setStatusCode(200);
            }
            else{
                return response()
                    ->json(['message' => 'failed'])
                    ->setStatusCode(202);
            }
        }
        else{
            return response()
                ->json(['message' => 'failed'])
                ->setStatusCode(202);
        }
    }
}
