<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Membership extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this['user']->id,
            'name'       => $this['user']->name,
            'email'      => $this['user']->email,
            'password'   => $this['user']->password,
            'phone'      => (int) $this['user']->phone,
            'attachment' => $this['user']->attachment,
            'type'       => $this['user']->type,
            'api_token'  => $this['user']->api_token,
            'member'     => $this['member'] ? $this['member'] : ''
        ];
    }
}
