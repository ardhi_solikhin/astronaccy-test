export const firstMixin =  { 
  methods: {
    stringOnly(event) {
      var inputValue = event.charCode
      if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault()
      }
    }
  }
}