const createState = () => ({
  dataUser: null
})

const getters = {

}

const mutations = {
  SET_USER_DATA(state, data) {
    state.dataUser = data
  }
}

const actions = {
  getdataUser(context, data) {
    context.commit('SET_USER_DATA', data)
  }
}

const userModule = () => ({
  namespaced: true,
  state: createState(),
  getters,
  mutations,
  actions
})

export default userModule