import Vuex from 'vuex'
import userModule from "~/store/user";

const createStore = () => {
  return new Vuex.Store({
    strict: true,
    modules: {
      user: userModule()
    }
  })
}

export default createStore